CC := gcc
all: client server clean

server: server.o io.o
	gcc server.o io.o -o server

client: client.o io.o game.o
	gcc client.o io.o game.o -o client

client.o: client.c client.h io.h game.h
	gcc -c client.c -o client.o

server.o: server.c server.h io.h
	gcc -c server.c -o server.o

io.o: io.c io.h
	gcc -c io.c -o io.o

game.o: game.c game.h
	gcc -c game.c -o game.o

.PHONY: clean

clean:
	rm -r *.o