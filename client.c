#include "client.h"
#include "io.h"
#include "game.h"

int flag_skipped;

void setup_sending(int *bssocket, int bport, struct sockaddr_in* bs_addr){
    int opt = 1;
    struct sockaddr_in my_addr;

    if(((*bssocket) = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP)) < 0){
        print_string("Socket set failed\n");
    }

    my_addr.sin_family = AF_INET;
    my_addr.sin_port = htons(bport);
    my_addr.sin_addr.s_addr = inet_addr("127.0.0.1");

    if(setsockopt(*bssocket, SOL_SOCKET, SO_BROADCAST , &opt, sizeof(opt)) < 0){
        print_string("Setsockfail\n");
        close(*bssocket);
        exit(1);
    }
    if(bind(*bssocket, (struct sockaddr*) &my_addr, sizeof(my_addr)) < 0){
        print_string("Bind fail send\n");
        close(*bssocket);
        exit(1);
    }
    (*bs_addr).sin_family = AF_INET;
    (*bs_addr).sin_port = htons(bport);
    (*bs_addr).sin_addr.s_addr = inet_addr("255.255.255.255");

}

void setup_reading(int* bsocket, int bport, struct sockaddr_in* b_addr){
    int opt = 1;

    if(((*bsocket) = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP)) < 0){
        print_string("Sock faild\n");
    }

    (*b_addr).sin_family = AF_INET;
    (*b_addr).sin_port = htons(bport);
    (*b_addr).sin_addr.s_addr = htonl(INADDR_BROADCAST);

    if(setsockopt(*bsocket, SOL_SOCKET, SO_REUSEPORT|SO_BROADCAST , &opt, sizeof(opt)) < 0){
        print_string("Setsock fail\n");
        close(*bsocket);
        exit(1);
    }
    if(bind(*bsocket, (struct sockaddr*) b_addr, sizeof(*b_addr))<0){
        print_string("Bind fail read\n");
        close(*bsocket);
        exit(1);
    }
}

int clear_buf(int bport, int* bsocket, struct sockaddr_in* b_addr){
    char buf[UDP_BUFSIZE];
    int addr_len = sizeof(*b_addr);
    if(recvfrom(*bsocket, buf, UDP_BUFSIZE, 0, (struct sockaddr*)b_addr, &addr_len) < 0){
        print_string("Recv faild");
        close(*bsocket);
        exit(0);
    }
    return 0;
}

void set_game_tables(int*** vertical, int*** horizontal, int*** score_board, int size){
    int i = 0, j = 0;
    *score_board = (int**)malloc(sizeof(int**)*size);
    for(i = 0; i < size; i++){
        (*score_board)[i] = (int*)malloc(sizeof(int*)*size);
        for(j = 0; j < size; j++)
            (*score_board)[i][j] = 0;
    }
    *vertical = (int**)malloc(sizeof(int**)*(size + 1));
    for(i = 0; i < size + 1; i++){
        (*vertical)[i] = (int*)malloc(sizeof(int*)*size);
        for(j = 0; j < size; j++)
            (*vertical)[i][j] = 0;
    }
    *horizontal = (int**)malloc(sizeof(int**)*size);
    for(i = 0; i < size; i++){
        (*horizontal)[i] = (int*)malloc(sizeof(int*)*(size + 1));
        for(j = 0; j < size + 1; j++)
            (*horizontal)[i][j] = 0;
    }
}
int get_apponent_id(int turn, int my_id, int group_size){
    int result = my_id - turn;
    if (result <= 0)
        return result + group_size;
    return result;
}

void AlrmSigHnd(int signo){
    print_string("Time out reached\n");
    flag_skipped = 1;
}

int main(int argc, char** argv){
    int server_socket, port_server, bsocket, team_count, stat;
    int bport, turn, gamestat, bssocket, my_id, winner;
    int **vertical, **horizontal, **score_board;
    struct sockaddr_in serv_addr, b_addr, bs_addr;
    sscanf(argv[1], "%d", &port_server);
    sscanf(argv[2], "%d", &team_count);
    set_game_tables(&vertical, &horizontal, &score_board, team_count);

    if((server_socket = socket(AF_INET, SOCK_STREAM, 0)) < 0){
        print_string("Socke faild\n");
    }
    
    serv_addr.sin_family = AF_INET;
    serv_addr.sin_port = htons(port_server);
    serv_addr.sin_addr.s_addr = inet_addr("127.0.0.1");

    if(connect(server_socket, (struct sockaddr*)&serv_addr, sizeof(serv_addr)) < 0){
        print_string("Connection faild\n");
        close(server_socket);
        exit(1);
    }
    print_string("connected to server\n");
    char buf[TCP_BUFSIZE];
    buf[0] = argv[2][0];
    if(send(server_socket, buf, TCP_BUFSIZE, 0) < 0)
        print_string("send faild\n");
    if((stat = recv(server_socket, buf, TCP_BUFSIZE, 0)) < 0){
        print_string("Recv Err from server\n");
        close(server_socket);
        exit(1);
    }else if(stat == 0){
        print_string("Server disconnected\n");
        close(server_socket);
        exit(1);
    }
    print_string("Joined game on : ");print_string(buf);print_string("\n");
    sscanf(buf,"%d,%d", &bport, &turn);
    print_string("You are player #");print_int(turn + 1);print_string("\n");
    print_string("Your input format is <direction(1/0)> <x(1-max)> <y(1-max)>\nHorizantal = 0\nVertial = 1\n");
    setup_reading(&bsocket, bport, &b_addr);
    my_id = turn + 1;
    // setup_sending(&bssocket, bport, &bs_addr);
    struct sigaction sa;
    memset(&sa, 0, sizeof(sa));
    sa.sa_handler = AlrmSigHnd;
    sigaction(SIGALRM, &sa, NULL);
    while((winner = end_of_game(score_board, team_count)) == 0){
        alarm(20);
        flag_skipped = 0;
        print_board(vertical, horizontal, score_board, team_count);
        if(turn == 0){
            gamestat = make_move(bport, &bssocket, my_id, &bs_addr, &vertical, &horizontal, &score_board, team_count);
            if(flag_skipped == 0)
                clear_buf(bport, &bsocket, &b_addr);
        }else{
            gamestat = get_move(bport, &bsocket, get_apponent_id(turn, my_id, team_count), &b_addr, &vertical, &horizontal, &score_board, team_count);
        }
        if(gamestat == GAME_ENDED){
            break;
        }else if(gamestat == GO_AGAIN){
            continue;
        }else{
            turn--;
            if(turn < 0)
                turn = team_count - 1;
        }
        alarm(0);
    }
    print_string("Player "); print_int(winner); print_string(" won the game!\n");
    if(winner == my_id){
        sprintf(buf, "f%d", port_server);
        if(send(server_socket, buf, TCP_BUFSIZE, 0) < 0){
            print_string("Faild to free port\n");
        }
    }
    close(server_socket);
    close(bsocket);
}