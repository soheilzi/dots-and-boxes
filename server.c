#include "server.h"
#include "io.h"

void setup_listener(int port, int *listen_socket){
    struct sockaddr_in serv_addr, client_addr;
    int client_addr_len = sizeof(client_addr);
    char str[10];

    serv_addr.sin_family = AF_INET;
    serv_addr.sin_port = htons(port);
    serv_addr.sin_addr.s_addr = inet_addr("127.0.0.1");

    if((*listen_socket = socket(AF_INET, SOCK_STREAM, 0)) < 0){
        print_string("Socket faild\n");
        exit(1);
    }
    
    if(bind(*listen_socket, (struct sockaddr *)&serv_addr, sizeof(serv_addr)) < 0){
        print_string("bind faild\n");
        close(*listen_socket);
        exit(1);
    }

    if(listen(*listen_socket, 5) < 0){
        print_string("listen faild\n");
        close(*listen_socket);
        exit(1);
    }
    print_string("Listening on port ");print_int(port);print_string("\n");
}

void close_all(struct Client* clients, int count_clients){
    int i;
    for(i = 0;i < count_clients - 1; i++)
        close(clients[i].sock);
}

void add_client(struct Client* clients, int* count_clients, int listen_socket){
    struct sockaddr_in client_addr;
    int client_addr_len = sizeof(client_addr), clientid;
    char str[10];
    if((clientid = accept(listen_socket, (struct sockaddr*)&client_addr, &client_addr_len)) < 0){
        print_string("accepting error\n");
        close_all(clients, *count_clients);
        close(listen_socket);
        exit(1);
    }
    (*count_clients)++;
    clients = (struct Client*)realloc(clients, sizeof(struct Client)*(*count_clients));
    clients[(*count_clients) - 1].sock = clientid;
    print_string("Client with socket ");print_int(clientid);print_string(" added\n");
}

void setup_select(fd_set *readfds, int *max_fd, int listen_socket, struct Client *clients, int count_clients){
    int i;
    FD_ZERO(readfds);
    FD_SET(listen_socket, readfds); 
    (*max_fd) = listen_socket; 
    for(i = 0; i < count_clients; i++){
        FD_SET(clients[i].sock, readfds);
        if ((*max_fd) < clients[i].sock)
            (*max_fd) = clients[i].sock;
    }
}

void disconnect(struct Client* clients, int i, int* count_clients,int* twoplayer,int* threeplayer,int* fourplayer){
    int j, temp = clients[i].sock;
    char str[10];
    close(clients[i].sock);
    for(j = 0; j < 2; j++){
        if(twoplayer[j] == clients[i].sock)
            twoplayer[j] = 0;
    }
    for(j = 0; j < 3; j++){
        if(threeplayer[j] == clients[i].sock)
            threeplayer[j] = 0;
    }
    for(j = 0; j < 4; j++){
        if(fourplayer[j] == clients[i].sock)
            fourplayer[j] = 0;
    }
    clients[i] = clients[(*count_clients) - 1];
    (*count_clients)--;
    print_int(temp);print_string(" got disconnected\n");
}

void handle_group(int* group, int player, int num_group, int* available_ports){
    int i = 0, avl_port;
    char buf[TCP_BUFSIZE], str[10];
    while(i < num_group && group[i] != 0){i++;}
    group[i] = player;
    i = 0;
    while(i < num_group && group[i] != 0){i++;}
    if(i != num_group)
        return;
    i = 0;
    while(i < MAX_TEAMS && available_ports[i] == 1){i++;}
    if(i == MAX_TEAMS){
        print_string("Can't allocate more ports\n");
        exit(0);
    }
    avl_port = i + STARTING_BROADCAST_PORT;
    available_ports[i] = 1;
    for(i = 0; i < num_group; i++){
        sprintf(buf,"%d,%d",avl_port, i);
        if(send(group[i], buf, TCP_BUFSIZE, 0) < 0){
            print_string("Faild sending to:");print_int(group[i]);print_string("\n");
            exit(1);
        }
        group[i] = 0;
    }
    print_string("Group created on port: ");
    print_int(avl_port);
    print_string(" with ");
    print_int(num_group);
    print_string(" players in it\n");
}

void free_port(char* buf, int* available_ports){
    int port;
    if(buf[0] != 'f'){
        print_string("Invalid command from client\n");
        return;
    }
    sscanf(buf, "f%d", &port);
    available_ports[port - STARTING_BROADCAST_PORT] = 0;
    print_string("Port ");print_int(port);print_string(" is free\n");
}

void client_handler(struct Client* clients, int i, int* count_clients,
            int* available_ports,int* twoplayer,int* threeplayer,int* fourplayer){
    int stat;
    char buf[TCP_BUFSIZE];
    char str[10];
    if((stat = recv(clients[i].sock, buf, TCP_BUFSIZE, 0)) < 0){
        print_string("Recv Err on : ");print_int(clients[i].sock);print_string("\n");
        close_all(clients, *count_clients);
        exit(1);
    }else if(stat == 0){
        disconnect(clients, i, count_clients, twoplayer, threeplayer, fourplayer);
        return;
    }
    switch (buf[0] - '0')
    {
    case 2:
        handle_group(twoplayer, clients[i].sock, 2,available_ports);
        break;
    case 3:
        handle_group(threeplayer, clients[i].sock, 3, available_ports);
        break;
    case 4:
        handle_group(fourplayer, clients[i].sock, 4, available_ports);
    default:
        free_port(buf, available_ports);
    }
}   

int main(int argc, char** argv){
    int listen_socket, max_fd = 0, port, count_clients = 0;
    struct Client *clients = (struct Client*)malloc(sizeof(struct Client));
    int i;
    int available_ports[MAX_TEAMS];for(i = 0; i < MAX_TEAMS; i++){available_ports[i] = 0;}
    int twoplayer[2]; for(i = 0; i < 2; i++){twoplayer[i] = 0;}
    int threeplayer[3]; for(i = 0; i < 3; i++){threeplayer[i] = 0;}
    int fourplayer[4]; for(i = 0; i < 4; i++){fourplayer[i] = 0;}
    sscanf(argv[1], "%d", &port);

    setup_listener(port, &listen_socket);
    
    fd_set readfds;

    while(1){
        setup_select(&readfds, &max_fd, listen_socket, clients, count_clients);
        if(select(max_fd + 1, &readfds, NULL, NULL, NULL) < 0){
            print_string("Select fail\n");
            close_all(clients, count_clients);
            close(listen_socket);
            exit(1);
        }
        if(FD_ISSET(listen_socket, &readfds)){
            add_client(clients, &count_clients, listen_socket);
        }else{
            for(i = 0; i < count_clients; i++){
                if(FD_ISSET(clients[i].sock, &readfds)){
                    client_handler(clients, i, &count_clients, available_ports,
                                         twoplayer, threeplayer, fourplayer);
                    break;
                }
            }
        }
    }

    close(listen_socket);
}