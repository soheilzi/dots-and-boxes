#include "io.h"

void print_string(char* out){
    for(int i = 0; out[i] != '\0'; i++){
        write(STDOUT_FILENO, &out[i], 1);
    }
}

void print_int(int i){
    char str[15];
    sprintf(str, "%d", i);
    print_string(str);
}

int scan(char* in){
    // char* ch;
    // int size = 0;
    // do{
    //     read(STDIN_FILENO, ch, 1);
    //     in[size] = *ch;
    //     size++;
    // }while(*ch != '\n');
    // in[size - 1] = '\0';
    return read(STDIN_FILENO, in, MAX_INPUT_SIZE);
}