#ifndef __GARD_GAME
#define __GARD_GAME
#include <sys/types.h>
#include <stdio.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <errno.h>
#include <stdlib.h>
#include <string.h>
#include <arpa/inet.h>
#include <stdlib.h>
#include <unistd.h>

#define INVALID_NOT_IN_RANGE 1
#define INVALID_OCCUPIED 2
#define VERTICAL 1
#define HORIZONTAL 0
#define SCORE 1
#define NO_SCORE 0

#define GO_AGAIN 2
#define GAME_ENDED 1

int make_move(int bport, int* bssocket, int player_id, struct sockaddr_in* bs_addr, int*** vertical, int*** horizontal, int*** score_board, int size);
int get_move(int bport, int* bsocket, int player_id, struct sockaddr_in* b_addr, int*** vertical, int*** horizontal, int*** score_board, int size);
void print_board(int** vertical, int** horizontal, int** score_board, int size);
int end_of_game(int **score_board, int size);

#endif