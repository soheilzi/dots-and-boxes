#ifndef __GARD_CLIENT
#define __GARD_CLIENT
#include <sys/types.h>
#include <stdio.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <errno.h>
#include <stdlib.h>
#include <string.h>
#include <arpa/inet.h>
#include <stdlib.h>
#include <unistd.h>
#include <signal.h>

void setup_reading(int* bsocket, int bport, struct sockaddr_in* b_addr);
void setup_sending(int *bssocket, int bport, struct sockaddr_in* bs_addr);

#endif