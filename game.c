#include "game.h"
#include "io.h"
#include "client.h"

int end_of_game(int **score_board, int size){
	int i, j, flag = 0;
	int scores[5];
	int winner = 1;
	int winner_score = 0;
	for(i = 0; i < size; i++){
		for(j = 0; j < size; j++){
			if(score_board[i][j] == 0)
				flag = 1;
			else
				scores[score_board[i][j]]++;
			if(scores[score_board[i][j]] > winner_score){
				winner_score = scores[score_board[i][j]];
				winner = score_board[i][j];
			}
		}
	}
	if(flag == 1)
		return 0;
	return winner;
}

void print_board(int** vertical, int** horizontal, int** score_board, int size){
	char board[50][50];
	int i, j;
	char ch;
	for(i = 0; i < size + 1; i++){
		for(j = 0; j < size + 1; j++){
			board[i*2][j*2] = '*';
		}
	}
	for(i = 0; i < size + 1; i++){
		for(j = 0; j < size; j++){
			if(horizontal[j][i] == 1)
				ch = '-';
			else
				ch = ' '; 
			board[i*2][j*2 + 1] = ch;
		}
	}
	for(i = 0; i < size; i++){
		for(j = 0; j < size + 1; j++){
			if(vertical[j][i] == 1)
				ch = '|';
			else
				ch = ' '; 
			board[i*2+ 1][j*2] = ch;
		}
	}
	for(i = 0; i < size; i++){
		for(j = 0; j < size; j++){
			ch = '0' + score_board[j][i];
			board[i*2 + 1][j*2 + 1] =(score_board[j][i] == 0)?' ':ch;
		}
	}
	for(i = 0; i < ((size * 2) + 1); i++){
		board[i][size * 2 + 1] = '\0';
	}
	for(i = 0; i < ((size * 2) + 1); i++){
		print_string(board[i]);
		print_string("\n");
	}

}

int validate_input(int** vertical, int** horizontal, int direction, int x, int y,int size){
	if (direction >= 0 && direction <= 1){
		switch (direction){
		case VERTICAL:
			if (x <= size + 1 && x >= 1 && y <= size && y >= 1){
				if (vertical[x - 1][y - 1] == 0)
					return 0;
				else
					return INVALID_OCCUPIED;
			}else
				return INVALID_NOT_IN_RANGE;
			break;
		case HORIZONTAL:
			if (x <= size && x >= 1 && y <= size + 1 && y >= 1){
				if (horizontal[x - 1][y - 1] == 0)
					return 0;
				else
					return INVALID_OCCUPIED;
			}else
				return INVALID_NOT_IN_RANGE;
			break;
		}
	}
	else{
		return INVALID_NOT_IN_RANGE;
	}
}

int score_updater(int player_id, int** score_board, int** vertical, int** horizontal, int size){
	int i, j,flag=NO_SCORE;
	for (i = 0; i < size; i++){
		for ( j = 0; j < size; j++){
			if (vertical[j][i] == 1 && vertical[j + 1][i] == 1 && horizontal[j][i] == 1 && horizontal[j][i + 1] == 1 && score_board[j][i] == 0) {
				score_board[j][i] = player_id;
				flag=SCORE;
			}
		}
	}
	if (flag == SCORE){
		return SCORE;
	}
	return NO_SCORE;
}

int move_maker(int player_id, int direction, int y, int x, int*** vertical, int*** horizontal, int*** score_board, int size){

	switch (direction){
	case VERTICAL:
		(*vertical)[x - 1][y - 1] = 1;
		break;
	case HORIZONTAL:
		(*horizontal)[x - 1][y - 1] = 1;
		break;
	}
	if(score_updater(player_id, *score_board, *vertical, *horizontal, size) == SCORE)
		return GO_AGAIN;
	return 0;
}

char* get_and_validate_input(int** vertical, int** horizontal, int** score_board, int size){
	int x, y, direction;
	char* buf;
	buf = (char*)malloc(sizeof(char*)*UDP_BUFSIZE);
	scan(buf); 
	sscanf(buf, "%d,%d,%d", &direction, &y, &x);
	while (validate_input(vertical, horizontal, direction, x, y, size))/*this loop validates the input*/
	{
		switch (validate_input(vertical, horizontal, direction, x, y, size))
		{
		case INVALID_NOT_IN_RANGE:
			print_string("error: Input not in range please re enter the coordinates:\n");
			break;
		case INVALID_OCCUPIED:
			print_string("error: Input is already occupied.Please re enter the coordinates:\n");
			break;
		case -1:
			break;
		}
    	scan(buf); 
		sscanf(buf, "%d,%d,%d", &direction, &y, &x);
	}
	return buf;
}

int make_move(int bport, int* bssocket, int player_id, struct sockaddr_in* bs_addr,
                     int*** vertical, int*** horizontal, int*** score_board, int size){
    char buf[UDP_BUFSIZE];
	int move_result, direction, y, x, k;
	// scanf("%d,%d,%d", &direction, &y, &x); 
	// buf = get_and_validate_input(*vertical, *horizontal, *score_board, size);
	print_string("It's your turn\n");
	if(scan(buf) < 0){
		print_string("You have skipped\n");
		return 0;
	}
	sscanf(buf, "%d,%d,%d", &direction, &y, &x);
	move_result = move_maker(player_id, direction, y, x, vertical, horizontal, score_board, size);
    setup_sending(bssocket, bport, bs_addr);
    if(sendto(*bssocket, buf, UDP_BUFSIZE, 0, (struct sockaddr*)bs_addr, sizeof(*bs_addr)) < UDP_BUFSIZE){
        print_string("Sending Faild\n");
        close(*bssocket);
        exit(1);
    }
    print_string("sent ");print_string(buf);print_string("\n");
    close(*bssocket);
    return move_result;
}


int get_move(int bport, int* bsocket, int player_id, struct sockaddr_in* b_addr, 
                    int*** vertical, int*** horizontal, int*** score_board, int size){
    char buf[UDP_BUFSIZE];
    int addr_len = sizeof(*b_addr);
	int move_result, direction, y, x;
	print_string("Waiting for player ");print_int(player_id);print_string(" to make their move\n");
    if(recvfrom(*bsocket, buf, UDP_BUFSIZE, 0, (struct sockaddr*)b_addr, &addr_len) < 0){
        print_string("Recv Timed out\n");
		return 0;
    }
    print_string("Got ");print_string(buf);print_string("\n");
	sscanf(buf, "%d,%d,%d", &direction, &y, &x);
	move_result = move_maker(player_id, direction, y, x, vertical, horizontal, score_board, size);
    return move_result;
}