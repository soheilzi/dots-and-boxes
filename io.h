#ifndef __IO_GARD
#define __IO_GARD

#include <sys/types.h>
#include <stdio.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <errno.h>
#include <stdlib.h>
#include <string.h>
#include <arpa/inet.h>
#include <stdlib.h>
#include <unistd.h>

#define MAX_INPUT_SIZE 512
#define TCP_BUFSIZE 40
#define UDP_BUFSIZE 40

void print_string(char*);
void print_int(int i);
int scan(char*);

#endif