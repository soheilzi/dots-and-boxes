#ifndef __GARD_SERVER
#define __GARD_SERVER
#include <sys/types.h>
#include <stdio.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <errno.h>
#include <stdlib.h>
#include <string.h>
#include <arpa/inet.h>
#include <stdlib.h>
#include <unistd.h>

#define MAX_TEAMS 20
#define STARTING_BROADCAST_PORT 8080

struct Client{
    int sock;
    int port;
    int num_team;
};
#endif